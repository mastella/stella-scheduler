package com.stella.framework.scheduler;

import java.util.Collection;

import com.stella.framework.job.Job;

public interface RunScheduler {
	
	public void addJob(Job job);
	public int numberOfJobs();
	public Collection<Job> getJobs();
	
	public void notifyJobCompletion(Job job);
	public void notifyJobReady(Job job);
	
	public void runBatch(final int numberOfConcurrentJobsAllowed);
}
