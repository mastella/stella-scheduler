package com.stella.framework.datastage;

import org.apache.log4j.Logger;

import com.stella.framework.job.JobImpl;
import com.stella.framework.job.JobStatus;

public class DatastageJobImpl extends JobImpl {

	private static final Logger logger = Logger.getLogger(DatastageJobImpl.class);
	
	private static final int SLEEP_TIME = 3000;
	
	public DatastageJobImpl(String name) {
		super(name);
	}

	@Override
	public JobStatus executeJob() {
		
		logger.info("dsjob -execute " + name);
		
		try {
			
			Thread.sleep(SLEEP_TIME);
			
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		}
		
		logger.info(name + " completed.");
		
		return JobStatus.SUCCESSFUL;
	}
}
