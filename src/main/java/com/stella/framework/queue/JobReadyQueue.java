package com.stella.framework.queue;

import java.util.Collection;

import com.stella.framework.job.Job;

public interface JobReadyQueue {
	
	public void addJob(Job job);
	public boolean hasNextJob();
	public Job getNextJob();
	public int numberOfJobs();
	public Collection<Job> peekAtJobs();
}
