package com.stella.framework.queue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;
import com.stella.framework.job.Job;

public class FifoQueueImpl implements JobReadyQueue {

	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(FifoQueueImpl.class);
	
	private Queue<Job> queue;
	private int currentQueueSize = 0;
	
	public FifoQueueImpl() {
		queue = new LinkedList<Job>();
	}
	
	public synchronized int numberOfJobs() {
		return currentQueueSize;
	}
	
	public synchronized void addJob(Job job) {
		currentQueueSize++;
		queue.add(job);
	}

	public synchronized boolean hasNextJob() {
		return queue.isEmpty();
	}

	public synchronized Job getNextJob() {
		
		if(queue.isEmpty() == false) {
			currentQueueSize--;
			return queue.poll();
		}
		return null;
	}

	public synchronized Collection<Job> peekAtJobs() {
		
		Iterator<Job> iter = queue.iterator();
		List<Job> list = new ArrayList<Job>();
		
		while(iter.hasNext()) {
			list.add(iter.next());
		}
		return list;
	}
}
