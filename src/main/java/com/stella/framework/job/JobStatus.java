package com.stella.framework.job;

public enum JobStatus {
	
	RUNNING(0),
	QUEUED(1),
	ABORTED(3),
	WAITING(4),
	SUCCESSFUL(5),
	BLOCKED(6),
	READY(7),
	EXCEPTION_THROWN(100),
	UNKNOWN(99);
	
	private int status;
	
	private JobStatus(int status) {
		this.status = status;
	}
	
	public static JobStatus getJobStatusFromInt(int status) {
		
		for(JobStatus jobStatus : JobStatus.values()) {
			if(status == jobStatus.status) {
				return jobStatus;
			}
		}		
		
		return JobStatus.UNKNOWN;
	}
}
