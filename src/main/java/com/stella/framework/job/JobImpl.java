package com.stella.framework.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.stella.framework.scheduler.RunScheduler;

public abstract class JobImpl implements Job, Comparable<JobImpl> {

	private static final Logger logger = Logger.getLogger(JobImpl.class);
	private static AtomicInteger nextId = new AtomicInteger();
	
	private final int jobId = nextId.incrementAndGet();
	
	protected String name;
	protected int weight;
	private JobStatus jobStatus;
	private RunScheduler runScheduler;
	
	private Map<String, JobImpl> blockingJobs;
	private Collection<JobImpl> dependantJobs;
	
	public JobImpl(final String name) {
		
		this.name = name;
		
		blockingJobs = new HashMap<String, JobImpl>();
		dependantJobs = new ArrayList<JobImpl>();
		
		jobStatus = JobStatus.READY;
		weight = 0;
	}
	
	public int getUniqueId() {
		return jobId;
	}
	
	public JobImpl(final String name, final int weight) {
		this(name);
		this.weight = weight;
	}
	
	public void setRunScheduler(RunScheduler runScheduler) {
		this.runScheduler = runScheduler;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public String getName() {
		return name;
	}
	
	public synchronized JobStatus getStatus() {
		return jobStatus;
	}
	
	protected void addBlockingJob(JobImpl jobImpl) {
		blockingJobs.put(jobImpl.getName(), jobImpl);
		jobStatus = JobStatus.WAITING;
	}
	
	public Job addDependantJob(Job job) {
		if(job instanceof JobImpl) {
			return addDependantJob((JobImpl)job);
		}
		return null;
	}
	
	public JobImpl addDependantJob(JobImpl jobImpl) {
		
		if(jobImpl != this) {
			dependantJobs.add(jobImpl);
			jobImpl.addBlockingJob(this);
		}
		return this;
	}
	
	public Collection<JobImpl> getDependantJobs() {
		return Collections.unmodifiableCollection(dependantJobs);
	}
	
	public void notifyDependants() {
		for(JobImpl dependantJob : dependantJobs) {
			dependantJob.update(this);
		}
	}
	
	public void setStatus(JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}
	
	protected synchronized void update(JobImpl predecessorJob) {
		
		if(jobStatus != JobStatus.WAITING) {
			return;
		}
		
		final JobStatus predecessorJobStatus = predecessorJob.getStatus();
		
		if(predecessorJobStatus == JobStatus.SUCCESSFUL) {
			
			blockingJobs.remove(predecessorJob.getName());
			
			if(blockingJobs.isEmpty()) {
				jobStatus = JobStatus.READY;
				runScheduler.notifyJobReady(this);				
			}
			
		} else if(predecessorJobStatus == JobStatus.ABORTED) {
			jobStatus = JobStatus.BLOCKED;
		}
		
		logger.info(name + " = " + jobStatus);
	}
	
	public void execute() {
		
		try {
			
			jobStatus = JobStatus.RUNNING;
			jobStatus = executeJob();
			
		} catch(Exception e) {
			logger.error(name + " has thrown exception: " + e.getMessage());
			jobStatus = JobStatus.EXCEPTION_THROWN;
		}
	}
	
	public int compareTo(JobImpl jobImpl) {
		return weight > jobImpl.weight ? 1 : weight < jobImpl.weight ? -1 : 0;
	}
	
	public abstract JobStatus executeJob();
	
	@Override
	public String toString() {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(name + ": {");

		for(JobImpl jobImpl : dependantJobs) {
			stringBuilder.append(jobImpl.getName() + "|");
		}
		stringBuilder.append(" }");
		
		return stringBuilder.toString();
	}
}
