package com.stella.framework.job;

import org.apache.log4j.Logger;

import com.stella.framework.scheduler.RunScheduler;

public final class JobThread implements Runnable {

	private static final Logger logger = Logger.getLogger(JobThread.class);
	
	private Job job;
	private RunScheduler runScheduler;
	
	public JobThread(RunScheduler runScheduler, Job job) {
		
		this.runScheduler = runScheduler;
		this.job = job;
	}
	
	public Job getJob() {
		return job;
	}
	
	public JobStatus getJobStatus() {
		return job.getStatus();
	}
	
	public void run() {
		
		logger.debug(job.getName() + " about to execute");
		
		job.execute();
		
		job.notifyDependants();

		runScheduler.notifyJobCompletion(job);
	}
}
