package com.stella.framework.job;

import com.stella.framework.scheduler.RunScheduler;

public interface Job {

	public int getUniqueId();
	public int getWeight();
	public String getName();
	
	public JobStatus getStatus();
	public void setStatus(JobStatus jobStatus);

	public void execute();
	public Job addDependantJob(Job job);
	
	public void notifyDependants();
	public void setRunScheduler(RunScheduler runScheduler);
}
