package com.stella.api;

import java.lang.reflect.Method;

import org.reflections.Reflections;

import java.util.Arrays;
import java.util.Set;

import org.apache.log4j.Logger;

public class BootStrap {

	private static Logger logger = Logger.getLogger(BootStrap.class);

	private static Class<RunnableTask> interfaceToRun = RunnableTask.class;
	private static final String runTaskMethod = "runTask";
	private static final String packagePath = "com.stella.tasks";
	
	public static void usage() {

		logger.error("Usage:");
		logger.error(" arg0 = path to java runnable class");
		logger.error(" argX = arguments to the task");
		
		logger.info("");
		logger.info("Current tasks defined:");
		
		for (Class<? extends RunnableTask> klass : getRunnableTasks()) {
			logger.info(" " + klass.getSimpleName());
		}
		System.exit(1);
	}

	private static Set<Class<? extends RunnableTask>> getRunnableTasks() {
		
		Reflections reflections = new Reflections(packagePath);
		return reflections.getSubTypesOf(RunnableTask.class);
	}
	
	public static void main(String[] args) {

		if (args.length < 1) { 
			usage();	
		}

		try {

			String className = args[0].contains(".") ? args[0] : (packagePath + "." + args[0]);

			ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();

			Class<?> klass = myClassLoader.loadClass(className);

			logger.info("found class with SimpleName: " + klass.getSimpleName());
			
			for(@SuppressWarnings("rawtypes") Class c : klass.getInterfaces()) {
				
				if(c.equals(interfaceToRun)) {
					
					Method runMethod = klass.getDeclaredMethod(runTaskMethod, new Class[] { String[].class });
					Object object = klass.newInstance();
					runMethod.invoke(object, new Object[] { Arrays.copyOfRange(args, 1, args.length) });
					System.exit(0);
				}
			}
			
			logger.error("specified class is not a RunableTask");
			
		} catch (Exception e) {

			logger.error(e.getMessage());
			
			Throwable throwable = e.getCause();
			
			logger.error("Exception was thrown in RunTask.");
			logger.error("Message:" + throwable.getMessage());
			
			if(logger.isDebugEnabled()) {
				for (StackTraceElement stackTraceElement : throwable.getStackTrace()) {
					logger.warn(stackTraceElement.toString());
				}
			}
		}
		System.exit(1);
	}
}
