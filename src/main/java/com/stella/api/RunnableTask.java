package com.stella.api;

import com.stella.exceptions.RunTaskException;

public interface RunnableTask {
	public void runTask(final String [] args) throws RunTaskException;
}
