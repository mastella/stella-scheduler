package com.stella.exceptions;

public class RunTaskException extends Exception {

	private static final long serialVersionUID = 1L;

	public RunTaskException(String message) {
		super(message);
	}
	
	public RunTaskException(Throwable cause) {
		super(cause);
	}
}
