package com.stella.tasks;

import org.apache.log4j.Logger;

import com.stella.api.RunnableTask;
import com.stella.exceptions.RunTaskException;
import com.stella.framework.datastage.DatastageJobImpl;
import com.stella.framework.job.JobImpl;
import com.stella.framework.queue.FifoQueueImpl;
import com.stella.framework.queue.JobReadyQueue;
import com.stella.framework.scheduler.RunScheduler;
import com.stella.framework.scheduler.RunSchedulerImpl;

public class TestSchedulerImpl implements RunnableTask {

	private Logger logger = Logger.getLogger(TestSchedulerImpl.class);
	
	public void runTask(String[] args) throws RunTaskException {
		
		JobReadyQueue jobQueue = new FifoQueueImpl();
		
		RunScheduler runScheduler = new RunSchedulerImpl(jobQueue);
		
		final int testJobcount = 10;
		
		JobImpl jobImpls[] = new JobImpl[testJobcount];
		
		for(int i = 0; i < testJobcount; i++) {
			jobImpls[i] = new DatastageJobImpl("job" + i);
			runScheduler.addJob(jobImpls[i]);
		}
		
		jobImpls[0].addDependantJob(jobImpls[2]);
		jobImpls[1].addDependantJob(jobImpls[2]);
		jobImpls[2].addDependantJob(jobImpls[3]);
		
		jobImpls[3].addDependantJob(jobImpls[4])
				.addDependantJob(jobImpls[5])
				.addDependantJob(jobImpls[6])
				.addDependantJob(jobImpls[7]);

		jobImpls[4].addDependantJob(jobImpls[8]);
		jobImpls[5].addDependantJob(jobImpls[8]);
		jobImpls[6].addDependantJob(jobImpls[8]);
		jobImpls[7].addDependantJob(jobImpls[8]);
		
		jobImpls[8].addDependantJob(jobImpls[9]);
		
		logger.info("kicking off batch");
		runScheduler.runBatch(3);
		logger.info("completed");
	}
}
