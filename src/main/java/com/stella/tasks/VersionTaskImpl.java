package com.stella.tasks;

import org.apache.log4j.Logger;

import com.stella.api.RunnableTask;
import com.stella.exceptions.RunTaskException;

public class VersionTaskImpl implements RunnableTask {

	private Logger logger = Logger.getLogger(VersionTaskImpl.class);
	
	public void runTask(String[] args) throws RunTaskException {
		logger.info("Version : 1");
	}
}
